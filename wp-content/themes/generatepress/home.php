<?php
/*
     * Template Name: My HTML Homepage
     */
?>
<!-- This page is generated outside of WP by: /wp-content/themes/CURRENT-THEME/home.php -->
<!-- Your HTML Code Here -->

<!DOCTYPE html>
<html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src=https://www.googletagmanager.com/gtag/js?id=UA-196439446-1></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-196439446-1');
	</script>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="trening programi, letnji, python, data analyst, software tester, android developer, web developer">
	<link rel="apple-touch-icon" sizes="57x57" href="/assets/images/webicons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/assets/images/webicons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/webicons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/assets/images/webicons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/images/webicons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/assets/images/webicons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/assets/images/webicons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/assets/images/webicons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/assets/images/webicons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/assets/images/webicons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/images/webicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/assets/images/webicons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/images/webicons/favicon-16x16.png">
	<meta name="msapplication-TileColor" content="#0071BB">
	<meta name="msapplication-TileImage" content="/assets/images/webicons/ms-icon-144x144.png">
	<meta name="theme-color" content="#0071BB">
	<!-- OG -->
	<meta property="og:title" content="Trening programi - Semos education">
	<meta property="og:site_name" content="stipendije.semosedu.rs">
	<meta property="og:url" content="http://stipendije.semosedu.rs/">
	<meta property="og:description" content="">
	<meta property="og:type" content="website">
	<meta property="og:image" content="/assets/images/bg_preview-new.png">
	<!-- OG -->

	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="assets/css/global.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<title>Letnji trening programi - Semos Edukacija</title>
</head>

<body>
	<header>
		<!-- Facebook Pixel Code -->
		<script>
			! function(f, b, e, v, n, t, s) {
				if (f.fbq) return;
				n = f.fbq = function() {
					n.callMethod ?
						n.callMethod.apply(n, arguments) : n.queue.push(arguments)
				};
				if (!f._fbq) f._fbq = n;
				n.push = n;
				n.loaded = !0;
				n.version = '2.0';
				n.queue = [];
				t = b.createElement(e);
				t.async = !0;
				t.src = v;
				s = b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t, s)
			}(window, document, 'script',
				'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '949668935784164');
			fbq('track', 'PageView');
		</script>
		<noscript>
			<img height="1" width="1" src="https://www.facebook.com/tr?id=949668935784164&ev=PageView
		&noscript=1" />
		</noscript>
		<!-- End Facebook Pixel Code -->

		<div class="header-wrapper dekstop-header-wrapper">
			<div class="logo-holder">
				<div class="logo-wrap">
					<a href="/"><img src="assets/images/semos_edu_logo_black.svg" /></a>
				</div>
			</div>
			<div class="nav-holder">
				<div class="menu-toggle"><span id="mobile-menu-toggle"><i class="fa fa-bars"></i></span></div>
				<nav>
					<ul>
						<li><a href="#" data-link="why-apply">Zašto da se prijaviš</a></li>
						<li>
							<div class="dropdown-wrap">
								<a href="#" data-link="programs-details">Trening programi</a>
								<div class="arrow-up"></div>
								<div class="dropdown-content">
									<ul>
										<a href="azure.html">
											<li>Microsoft Azure Administrator</li>
										</a>
										<a href="web-developer.html">
											<li>Web Developer - Microsoft</li>
										</a>
										<a href="software-tester.html">
											<li>Software Tester - ISTQB</li>
										</a>
										<a href="game-developer.html">
											<li>Game Developer - Unity</li>
										</a>
									</ul>
								</div>
							</div>
						</li>
						<li><a href="#" data-link="what-you-get">Benefiti</a></li>
						<li><a href="#" data-link="how-to-apply">Proces prijave</a></li>
						<li><a href="#" data-link="success-stories">Iskustva klijenata</a></li>
						<li><a href="#" data-link="dmnsection">Uspešna partnerstva</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>

	<section id="hero">
		<div class="wrapper hero-wrapper">
			<div class="hero-content">
				<span class="hero-text">ove godine <b>semos education</b> <br>zajedno sa partnerima organizuje</span>
				<h1 class="hero-heading">letnje ZVANIČNE trening programe<br /> </h1>
				<span class="hero-text">Prijavi se za neki od trening programa i uči uz podršku najboljih trenera!</span>
				<div class="hero-cta">
					<a href="#" class="button blue" data-link="why-apply">Saznaj više</a>
					<!-- <a href="https://forms.office.com/Pages/ResponsePage.aspx?id=p5Vnext2r0ahdypzIE-ANo6w5NaTJYhNquziwaRmgZNUOUZaVUg5SlJEOUdNUzRTMEJIUERXSDhNViQlQCN0PWcu" target="_blank" class="button ghost">Prijavi se</a> -->

					<a href="#" class="button ghost" data-link="programs-details">Programi</a>
				</div>
			</div </div>
	</section>

	<section id="partners">
		<div class="wrapper partners-wrapper">
			<span class="section-title">Naši partneri</span>
			<div class="partners-logos-wrap">
				<a href="https://semosedu.com.mk/%D0%9F%D0%BE%D1%87%D0%B5%D1%82%D0%BD%D0%B0/%D0%97%D0%B0_%D0%BD%D0%B0%D1%81/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B5%D1%82%D0%B5%D0%BD%D1%86%D0%B8%D0%B8.aspx">
					<img src="assets/images/partner_logos/microsoft_logo.png">
				</a>
				<a href="https://semosedu.com.mk/%D0%9F%D0%BE%D1%87%D0%B5%D1%82%D0%BD%D0%B0/%D0%97%D0%B0_%D0%BD%D0%B0%D1%81/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B5%D1%82%D0%B5%D0%BD%D1%86%D0%B8%D0%B8.aspx">
					<img src="assets/images/partner_logos/isqi_logo.png">
				</a>
				<a href="https://semosedu.com.mk/%D0%9F%D0%BE%D1%87%D0%B5%D1%82%D0%BD%D0%B0/%D0%97%D0%B0_%D0%BD%D0%B0%D1%81/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B5%D1%82%D0%B5%D0%BD%D1%86%D0%B8%D0%B8.aspx">
					<img src="assets/images/partner_logos/council_logo.png">
				</a>
				<a href="https://semosedu.com.mk/%D0%9F%D0%BE%D1%87%D0%B5%D1%82%D0%BD%D0%B0/%D0%97%D0%B0_%D0%BD%D0%B0%D1%81/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B5%D1%82%D0%B5%D0%BD%D1%86%D0%B8%D0%B8.aspx">
					<img src="assets/images/partner_logos/cisco_logo.png">
				</a>
				<a href="https://semosedu.com.mk/%D0%9F%D0%BE%D1%87%D0%B5%D1%82%D0%BD%D0%B0/%D0%97%D0%B0_%D0%BD%D0%B0%D1%81/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B5%D1%82%D0%B5%D0%BD%D1%86%D0%B8%D0%B8.aspx">
					<img src="assets/images/partner_logos/aws_logo.png">
				</a>
				<a href="https://semosedu.com.mk/%D0%9F%D0%BE%D1%87%D0%B5%D1%82%D0%BD%D0%B0/%D0%97%D0%B0_%D0%BD%D0%B0%D1%81/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B5%D1%82%D0%B5%D0%BD%D1%86%D0%B8%D0%B8.aspx">
					<img src="assets/images/partner_logos/adobe_logo.png">
				</a>
				<a href="https://semosedu.com.mk/%D0%9F%D0%BE%D1%87%D0%B5%D1%82%D0%BD%D0%B0/%D0%97%D0%B0_%D0%BD%D0%B0%D1%81/%D0%9A%D0%BE%D0%BC%D0%BF%D0%B5%D1%82%D0%B5%D0%BD%D1%86%D0%B8%D0%B8.aspx">
					<img src="assets/images/partner_logos/unity_logo.png">
				</a>
			</div>
		</div>
	</section>

	<!-- <section>
		<div class="wraper">
		<h2 class="section-title">Budi jedan od prvih 12 polaznika i poklanjamo ti stipendiju u iznosu od 50%</h2>
		</div>
	</section> -->

	<section id="why-apply">
		<div class="wrapper why-apply-wrapper">
			<h2 class="section-title"><i class="fa fa-arrow-right"></i> Budi jedan od prvih 12 polaznika i poklanjamo ti stipendiju u iznosu od 50%</h2>

			<h1>Zašto da se prijaviš?</h1>
			<p>Iskoristi jedinstvenu priliku da učiš po zvaničnim programima naših partnera. Uz sjajne trenere pripremi se za sertifikaciju, radi na realnom projektu i ono što je najbolje – budi jedan od izabranih koji će dobiti priliku da ode na praksu ili se zaposli u nekoj od vodećih IT kompanija koji su naši partneri i saradnici.</p>
			<div class="why-apply-reasons-wrap">
				<div class="why-apply-reason">
					<i class="fa fa-book reason-icon"></i>
					<h5>OFICIJALNI TRENINZI</h5>
					<p>Svi letnji trening programi su zvanični, akreditovani i sertifikovani od strane najvećih svetskih softverskih kompanija čiji smo ovlašćeni trening partneri: Microsoft, AWS, ISTQB, Unity. Sve obuke drže sertifikovani treneri sa višegodišnjim i uskustvom u praksi, a uz obuku dobijaš pristup aktuelnim, zvaničnim i odobrenim materijalima za učenje, i stičeš pravo za polaganje zvaničnih ispita za sticanje kompetencija.</p>
				</div>
				<div class="why-apply-reason">
					<i class="fa fa-certificate reason-icon"></i>
					<h5>PODRŠKA STRUČNJAKA</h5>
					<p>Radićeš na realnom projektu, a u tome će ti pomoći stručni treneri sa višegodišnjim iskustvom u oblasti koju predaju. Ono što svaki naš trening program čini posebnim je mogućnost za sticanje praktičnog iskustva i rad na realnom projektu.</p>
				</div>
				<div class="why-apply-reason">
					<i class="fa fa-star reason-icon"></i>
					<h5>LETNJI TRENING PROGRAMI</h5>
					<p>FIRST MINUTE PRICE – Ugrabi jedinstvenu priliku i upiši se na neki od zvaničnih letnjih treninga, po specijalnoj ceni! Pogledaj šta smo ti pripremili u okviru trening programa za koji se odlučiš. Potrebno je samo da se registruješ i započneš avanturu! </p>
				</div>
			</div>
		</div>
	</section>

	<section id="success-count">
		<div class="wrapper success-count-wrapper">
			<div class="count-wrap">
				<span class="count-number">4</span>
				<span class="count-text">Najpopularnije IT profesije</span>
			</div>
			<div class="count-wrap">
				<span class="count-number">10+</span>
				<span class="count-text">Sati rada na praktičnim projektima</span>
			</div>
			<div class="count-wrap">
				<span class="count-number">100%</span>
				<span class="count-text">Zvanični sertifikovani trening program</span>
			</div>
			<div class="count-wrap">
				<span class="count-number">800+</span>
				<span class="count-text">Naših zadovoljnih poslovnih klijenata</span>
			</div>
		</div>
	</section>

	<section id="programs-details">
		<div class="wrapper programs-details-wrapper">
			<h1>TRENING PROGRAMI:</h1>
			<table>
				<tr>
					<td>SOFTWARE TESTER – ISTQB</td>
					<td>
						<a href="software-tester.html"><span>saznaj više</span></a>
						<a href="https://forms.office.com/pages/responsepage.aspx?id=p5Vnext2r0ahdypzIE-ANuoRnDCZaopDh-ld2AaPW7pUME1DUzRFQUs2TFY2NDRURzNaNTlCS0xSRiQlQCN0PWcu"><span>prijavi se</span></a>
					</td>
				</tr>
				<tr>
					<td>AZURE ADMINISTRATOR – Microsoft</td>
					<td>
						<a href="azure.html"><span>saznaj više</span></a>
						<a href="https://forms.office.com/pages/responsepage.aspx?id=p5Vnext2r0ahdypzIE-ANuoRnDCZaopDh-ld2AaPW7pUNk4xQlBJNkg3U0lZS0YxTjAwR0RBOVk4TCQlQCN0PWcu"><span>prijavi se</span></a>
					</td>
				</tr>
				<tr>
					<td>WEB DEVELOPER – Microsoft</td>
					<td>
						<a href="web-developer.html"><span>saznaj više</span></a>
						<a href="https://forms.office.com/pages/responsepage.aspx?id=p5Vnext2r0ahdypzIE-ANuoRnDCZaopDh-ld2AaPW7pUOEhJRzdCSlQ4MzRDUlRTRzNSVjIyVENQSyQlQCN0PWcu"><span>prijavi se</span></a>
					</td>
				</tr>
				<tr>
					<td>GAME DEVELOPER - Unity</td>
					<td>
						<a href="game-developer.html"><span>saznaj više</span></a>
						<a href="https://forms.office.com/pages/responsepage.aspx?id=p5Vnext2r0ahdypzIE-ANuoRnDCZaopDh-ld2AaPW7pUQ0NHVzBCNkFKOUFTNTg3TjBIQVQ1STBSSyQlQCN0PWcu"><span>prijavi se</span></a>
					</td>
				</tr>
			</table>
			<!-- <div class="programs-cta-wrap">
				<a href="https://forms.office.com/Pages/ResponsePage.aspx?id=p5Vnext2r0ahdypzIE-ANo6w5NaTJYhNquziwaRmgZNUOUZaVUg5SlJEOUdNUzRTMEJIUERXSDhNViQlQCN0PWcu" class="button white" target="_blank">Prijavi se za stipendije</a>
				<!-- KOJ LINK DA BIDE TUKA, POSHO IMAAT POSEBNI -->
		</div> -->
		</div>
	</section>

	<section id="what-you-get">
		<div class="wrapper what-you-get-wrapper">
			<div class="what-you-get-heading-wrap">
				<!-- <img src="assets/images/lightbulb_icon.png"> -->
				<h1>Benefiti</h1>
				<div class="what-you-get-icons-wrap">
					<div><i class="fa fa-dollar"></i>
						<p>Prilika da pohađaš najpopularnije trening programe</p>
					</div>
					<div><i class="fa fa-graduation-cap"></i>
						<p>Zvanične obuke, materijali, labovi i mogućnost da se sertifikuješ</p>
					</div>
					<div><i class="fa fa-laptop"></i>
						<p>Iskustvo rada na praktičnim projektima</p>
					</div>
					<div><i class="fa fa-users"></i>
						<p>Mogućnost prakse u jednoj od najpoznatijih partnerskih IT kompanija</p>
					</div>
					<div><i class="fa fa-handshake-o"></i>
						<p>Online obuka koju možeš da pratiš sa svakog mesta</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="how-to-apply">
		<div class="wrapper how-to-apply-wrapper">
			<h1>Proces prijave</h1>
			<div class="how-to-wrapper">
				<div class="single-step">
					<span class="step-number white">1</span>
					<p>Odaberi program koji te zanima i upiši svoje podatke </p>
				</div>
				<div class="single-step">
					<span class="step-number white">2</span>
					<p>Dobićeš mejl sa daljim uputstvima i zakazaćemo informativni razgovor</p>

				</div>
				<div class="single-step">
					<span class="step-number white">3</span>
					<p>Tvoj letnji trening program može da počne! </p>
				</div>
			</div>
		</div>
	</section>

	<section id="success-stories">
		<div class="wrapper success-stories-wrapper">
			<h1>Iskustva klijenata:</h1>
			<div class="owl-carousel">
				<div class="story-card-holder">
					<img src="assets/images/alek.png">
					<p>„Još na samom početku, na prvom sastanku, osetio sam interesantno partnerstvo. Mi poštujemo i brinemo jedni za druge tako što uvek nalazimo način da pomirimo potrebe i mogućnosti. Njihova strast i agilnost u poslu su veoma važni za njihove partnere. Kad god započnemo pripreme za novi trening, ubeđen sam da će doći do win-win situacije." <br /><br /> <span class="story-author">- Mišo Janković, Head of Domain and Cloud Unit u Raiffeisen banci</span> </p>
				</div>
				<div class="story-card-holder">
					<img src="assets/images/miso.png">
					<p>„Odabir trenera, fleksibilnost prilikom ugovaranja termina treninga, angažovanje i entuzijazam od strane tima Semos Education-a, kao i dostavljanje propratnih materijala za učenje su bile stvari kojima sam bio najzadovoljniji u zajedničkoj saradnji. “ <br /><br /> <span class="story-author">- Aleksandar Milinković, Kancelarija za IT i eUpravu Republike Srbije</span> </p>
				</div>
				<div class="story-card-holder">
					<img src="assets/images/andjela.png">
					<p>„Obuka je bila odlična, veoma dobro organizovana i korisna. Naučila sam dosta novih stvari. Pored našeg instruktora, koji nam je dao gomilu dobrih saveta, najbolja stvar je bila to što smo nakon svake teme imali praktičan rad u lab-u i na taj način uvideli kako sve funkcioniše.“<br /><br /> <span class="story-author">- Anđela Dankić, GOWI</span> </p>
				</div>
			</div>
		</div>
	</section>
	<section id="dmnsection">
		<div class="wrapper success-stories-wrapper" style=max-width:100%;width:50%;>
			<h1>Uspešna partnerstva</h1><br /><br />
			<id=dmnid style="background-image:url('assets/images/logos.png');margin-left: -150px;background-size:contain;width:150%;height:450px;background-repeat:no-repeat;">
		</div>
	</section>
	<footer>
		<div class="wrapper footer-wrapper">
			<div class="footer-logo-wrap">
				<img src="assets/images/semos_edu_logo.svg">
			</div>
			<div class="footer-social-wrap">
				<span class="footer-icon"><a href="https://www.facebook.com/SemosEducationRS" target="_blank"><i class="fa fa-facebook"></i></a></span>
				<span class="footer-icon"><a href="https://www.instagram.com/semos.education.rs/" target="_blank"><i class="fa fa-instagram"></i></a></span>
				<span class="footer-icon"><a href="https://www.linkedin.com/showcase/semos-education-srbija/" target="_blank"><i class="fa fa-linkedin"></i></a></span>
				<span class="footer-icon"><a href="https://semosedu.com.mk/Pocetna.aspx" target="_blank"><i class="fa fa-globe"></i></a></span>
			</div>
			<div class="footer-links-wrap">
				<ul>
					<li><a href="https://semosedu.com.mk/Pocetna.aspx">semosedu.rs</a></li>
				</ul>
			</div>
		</div>
	</footer>
	<script src="https://use.fontawesome.com/5b964bf81e.js"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	<script>
		$(document).ready(function() {
			$(".owl-carousel").owlCarousel({
				responsive: {
					0: {
						items: 1
					}
				},
				nav: true
			});
		});
	</script>
	<script>
		// Scroll to script
		document.querySelectorAll('a[href="#"]').forEach(el => {
			if (el.dataset.link) {
				el.addEventListener('click', e => {
					e.preventDefault();
					let section = document.getElementById(el.dataset.link);
					let bbox = section.getBoundingClientRect();
					window.scrollTo({
						top: window.scrollY + bbox.top - 80,
						left: 0,
						behavior: 'smooth'
					});
				});
			}
		});
	</script>
	<script>
		document.querySelector("#mobile-menu-toggle").addEventListener('click', function() {
			if (document.querySelector('.nav-holder nav').classList.contains('active')) {
				document.querySelector('.nav-holder nav').classList.remove('active');
			} else {
				document.querySelector('.nav-holder nav').classList.add('active');
				document.querySelectorAll("nav.active ul li").forEach(el =>
					el.addEventListener('click', function() {
						document.querySelector('.nav-holder nav').classList.remove('active');
					})
				)
			}
		});
	</script>
</body>

</html>